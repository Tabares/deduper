package main

// This tool finds and exposes or deletes duplicated files in 2 or more directories
// Originally based on a collection of shell scripts I wrote
// One of the targets of this tool is not to use anything out of the stdlib

// TODO
// Make the program able to find duplicates (not delete!) in a single folder
// Use streaming for checksums to reduce memory usage
// Convert the checksumming to goroutine so we can run multiple ones
// (on the USB HDD I use, it's IO bound so it probably won't buy us anything)
// Allow choosing of different checksum algorithms

import (
	"crypto/md5"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

// file is used for containing file information
type file struct {
	path          string
	name          string
	size          int64
	checksum      string
	shortChecksum string
}

var verbose bool

// config contains settings for the program
type config struct {
	source        string
	comparison    []string
	outputFile    string
	delete        bool
	matchFilename bool
	matchMD5      bool
	verbose       bool
	separator     string
}

func main() {
	config, err := getConfig()

	if err != nil {
		exitWithError(err, 1, true)
	}

	if config.verbose {
		verbose = true
	}

	if verbose {
		fmt.Println("Comparing", config.source, "with", strings.Join(config.comparison, ", "))
	}

	err = verifyFolders(config.source, config.comparison)
	if err != nil {
		exitWithError(err, 4, false)
	}

	sourceFiles, err := getFilesData(config.source, config.matchMD5)
	if err != nil {
		exitWithError(errors.New("Error getting files from source directory '"+config.source+"' ("+err.Error()+")"), 3, false)
	}
	if len(sourceFiles) == 0 {
		exitWithError(errors.New("There are no files in source directory '"+config.source), 3, false)
	}

	if verbose {
		fmt.Println("Removing duplicate files from source list")
	}
	// deduplicate files to avoid checking the same file twice
	sourceFiles = removeDuplicates(sourceFiles, config.matchMD5)

	comparisonFiles := make([]file, 0)
	for _, v := range config.comparison {
		c, err := getFilesData(v, config.matchMD5)
		if err != nil {
			fmt.Println("Failed to get files from directory '" + v + "' (" + err.Error() + ")")
		}
		comparisonFiles = append(comparisonFiles, c...)
	}
	if len(comparisonFiles) == 0 {
		exitWithError(errors.New("There are no files in any of the comparison directories '"+strings.Join(config.comparison, ", ")), 3, false)
	}

	filesToDelete, err := findAllDuplicates(sourceFiles, comparisonFiles, config)
	if err != nil {
		exitWithError(err, 5, false)
	}

	for _, v := range filesToDelete {
		if config.delete {
			if verbose {
				fmt.Println("Deleting ", v.path)
				err := os.Remove(v.path)
				if err != nil {
					fmt.Println("Failed to remove file ", v.path, " - ", err.Error())
				}
			}
		} else {
			fmt.Println(v.checksum, "\t", v.path)
		}
	}
}

// getConfig extracts and validates command line options and builds a configuration struct
// returns a config struct
func getConfig() (config, error) {
	source := flag.String("s", "", "Source directory.")
	separator := flag.String("p", ",", "Separator (default ',').")
	comparison := flag.String("c", "", "Directories to compare (separated by specified separator).")
	outputFile := flag.String("o", "", "File to output the list of files to delete.")
	matchFilename := flag.Bool("f", false, "Match filenames.")
	matchMD5 := flag.Bool("m", false, "Match checksum.")
	verbose := flag.Bool("v", false, "Verbose reporting.")
	delete := flag.Bool("d", false, "Delete duplicated files")

	flag.Parse()
	var conf config
	conf.source = *source
	conf.comparison = make([]string, 0)
	conf.outputFile = *outputFile
	conf.matchFilename = *matchFilename
	conf.matchMD5 = *matchMD5
	conf.verbose = *verbose
	conf.separator = *separator
	conf.delete = *delete
	errorMessages := ""
	if conf.source == "" {
		errorMessages = "Source directory is required.\n"
	}

	if *comparison == "" {
		errorMessages += "At least one Comparison directory is required.\n"
	} else {
		conf.comparison = append(conf.comparison, strings.Split(*comparison, *separator)...)
	}

	if conf.matchFilename == false && conf.matchMD5 == false {
		errorMessages += "You need to specify one of -m or -f.\n"
	}

	if errorMessages != "" {
		fmt.Println(errorMessages)
		return conf, errors.New(errorMessages)
	}
	return conf, nil
}

// getFilesData recursively walks around a directory and records all the files it finds
// optionally, it also calculates the checksum for the file
// returns an array of file structs and the error state
func getFilesData(dir string, matchMD5 bool) ([]file, error) {
	files := make([]file, 0)

	fileData := func(path string, f os.FileInfo, err error) error {
		switch mode := f.Mode(); {
		case mode.IsDir():
			// do nothing
		case mode.IsRegular():
			var checksum string
			if matchMD5 {
				checksum, err = calculateFileChecksum(path)
				if err != nil {
					return errors.New("Failed to calculate the checksum for " + path)
				}
			}
			files = append(files, file{name: f.Name(), path: path, size: f.Size(), checksum: checksum})
		}
		return nil
	}

	f, err := os.Stat(dir)
	if err != nil {
		return nil, errors.New("Unable to find directory or file " + dir)
	}
	if !f.Mode().IsDir() {
		return nil, errors.New(dir + " is not a directory")
	}

	err = filepath.Walk(dir, fileData)

	return files, nil
}

// calculateFileChecksum calculates the checksum of a given file
// returns an string with the checksum and an error
// warning - reads the whole file in memory so don't use with huge files
func calculateFileChecksum(filePath string) (string, error) {
	var result []byte
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	// TODO - stream the file
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", hash.Sum(result)), nil
}

// findAllDuplicates finds all the files that are in both the sourceFiles and copies slices
// returns a slice of file structs
func findAllDuplicates(sourceFiles []file, copies []file, conf config) ([]file, error) {
	duplicates := make([]file, 0)
	for _, v := range sourceFiles {
		duplicates = append(duplicates, findDuplicates(v, copies, conf)...)
	}
	return duplicates, nil
}

// findDuplicates finds all duplicates of a given file, according to the config settings
// returns a slice of file structs
func findDuplicates(f file, comparison []file, conf config) []file {
	duplicates := make([]file, 0)

	for _, v := range comparison {
		// Files with different size cannot have the same contents
		if f.size == v.size {

			// Files with the same path are the same
			if f.path == v.path {
				continue
			}

			// If the path is the same, they are the same files
			if f.path == v.path {
				continue
			}

			// We're matching on filename
			if conf.matchFilename && f.name == v.name {
				duplicates = append(duplicates, v)
			}

			// We're matching on md5
			if conf.matchMD5 && f.checksum == v.checksum {
				duplicates = append(duplicates, v)
			}
		}
	}
	return duplicates
}

// verifyFolders checks that the source does not match the destination, otherwise you will be deleting the whole of the source
func verifyFolders(source string, comparison []string) error {

	for _, v := range comparison {
		if v == source {
			err := errors.New("ERROR: source and one of the directories to compare are the same (" + source + "). You really do not want to do that")
			return err
		}
	}
	return nil
}

// removeDuplicates removes duplicate entries from a list of files
func removeDuplicates(fileList []file, checkMD5 bool) []file {

	// TODO - implement function

	deduplicated := make([]file, len(fileList))
	var keys []string

	for _, v := range fileList {
		for i := range keys {
			addFile := true
			if checkMD5 {
				if v.checksum == keys[i] {
					addFile = false
					break
				}
			} else {
				if v.name == keys[i] {
					addFile = false
				}
			}
			if addFile {
				deduplicated = append(deduplicated, v)
			}
		}
	}
	// deduplicated := make([]file, len(fileList)) // Potentially over-allocating but this program is not that refined yet

	// // TODO change to in-place deduplication

	// return deduplicated
	return fileList
}

// printError prints the specified error message, the usage and then exits with the specified error code
func exitWithError(err error, errorCode int, showFlags bool) {
	fmt.Println(err)
	if showFlags {
		fmt.Println("Options: ")
		flag.PrintDefaults()
		// printUsage()
	}
	os.Exit(errorCode)
}
