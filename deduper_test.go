package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

type testFile struct {
	path     string
	content  string
	checksum string
}

func rootDir() string {
	return "deduper_tests"
}

func dirs() []string {
	rootDir := rootDir()
	return []string{
		rootDir + "/d1/",
		rootDir + "/d2/",
		rootDir + "/d3/",
	}
}

func files() []testFile {
	return []testFile{
		testFile{path: "/d1/1", content: "a"},
		testFile{path: "/d1/2", content: "b"},
		testFile{path: "/d1/3", content: "c"},
		testFile{path: "/d2/1", content: "a"},
		testFile{path: "/d2/2", content: "b"},
		testFile{path: "/d2/4", content: "d"},
		testFile{path: "/d3/1", content: "aa"},
		testFile{path: "/d3/2", content: "b"},
		testFile{path: "/d3/3", content: "cc"},
	}
}

func TestGetConfig(t *testing.T) {
	_, err := getConfig()
	fmt.Println(err.Error())
	if err.Error() != "Source directory is required.\n"+
		"At least one Comparison directory is required.\n"+
		"You need to specify one of -m or -f.\n" {
		t.Error("Config parsing failed")
	}
}

// TODO - fix hardcoded
func TestGetFilesData(t *testing.T) {
	getFilesData(dirs()[0], true)

}
func TestCalculateChecksum(t *testing.T) {

}

func setup() {

	rootDir := rootDir()

	os.Mkdir(rootDir, 0700)

	for _, v := range dirs() {
		os.Mkdir(v, 0700)
	}

	for _, v := range files() {
		fmt.Println(v.path)
		ioutil.WriteFile(rootDir+"/"+v.path, []byte(v.content), 0600)
	}
}

func teardown() {

	rootDir := rootDir()

	for _, v := range files() {
		os.Remove(rootDir + "/" + v.path)
	}

	for _, v := range dirs() {
		os.Remove(v)
	}

	os.Remove(rootDir)
}

func TestMain(m *testing.M) {
	setup()
	retCode := m.Run()
	teardown()
	os.Exit(retCode)
}
